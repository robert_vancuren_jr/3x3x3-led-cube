byte columns[] = {
  2,3,4,5,6,7,8,9,10
};

byte rows[] = {
  A2,A1,A0
};

const byte numberOfLEDs = 3*3*3;
byte pixels[numberOfLEDs];

int yPos = 0;

unsigned long then = 0;
int delayTime = 1000;

void setup() {
  Serial.begin(9600);
  int seed = analogRead(A5);
  Serial.println(seed); 
  Serial.println(LOW);
  Serial.println(HIGH);
  randomSeed(seed);
  for (int i = 0; i < 9; i++) {
    pinMode(columns[i], OUTPUT);
    digitalWrite(columns[i], LOW);
  }

  for (int i = 0; i < 3; i++) {
    pinMode(rows[i], OUTPUT);
    digitalWrite(rows[i], LOW);
  }

  for (int i = 0; i < numberOfLEDs; i++) {
    pixels[i] = LOW;
  }

  setPixel(0, 0, 0, HIGH);
  setPixel(1, 1, 1, HIGH);
  setPixel(1, 1, -1, HIGH);
  setPixel(-1, 1, 1, HIGH);
  setPixel(-1, 1, -1, HIGH);

  //lightLED(-1, 1, -1, 1000);
}

void loop() {
  
  unsigned long now = millis();
  if ((now - then) > delayTime) {
    clearAll();
    then = now;
    /*
    yPos += 1;
    if (yPos > 1) {
      yPos = -1;
    }


    int x = random(-1, 2);
    int y = random(-1, 2);
    int z = random(-1, 2);
    setPixel(x, y, z, HIGH);*/

    for (int x = -1; x <= 1; x++) {
      for (int y = -1; y <= 1; y++) {
        for (int z = -1; z <= 1; z++) {
          setPixel(x, y, z, random(0, 2));
        }
      }
    }

    delayTime = random(50, 501);
  }
 
  
  render();
  /*
  lightLED(0, 0, 0, 250);

  lightLED(0, -1, 0, 250);

  lightLED(0, 1, 0, 250);

  lightLED(1, 0, 0, 250);

  lightLED(-1, 0, 0, 250);

  lightLED(0, 0, 1, 250);

  lightLED(0, 0, -1, 250);


  lightLayerByLayer();
  delay(250);
  
  lightColumnByColumn();
  delay(250);
  

  oneByOne(50);
  delay(500);

  for (int i = 30; i >= 0; i -= 1) {
    oneByOne(i);
  }
  
  for (int i = 0; i < 5000; i++) {
    lightAllOneAtATime();
  }
  delay(500);
  */
}

void clearAll() {
  for (int i = 0; i < numberOfLEDs; i++) {
    pixels[i] = LOW;
  }
}

void render() {
  for (int i = 0; i < numberOfLEDs; i++) {
    //Serial.println(i);
    int value = pixels[i];
    int row = i / 9;
    int column = i - (row * 9);

    digitalWrite(columns[column], value);
    digitalWrite(rows[row], value);
    //delayMicroseconds(500);
    //delay(1000);
    digitalWrite(columns[column], LOW);
    digitalWrite(rows[row], LOW);
  }
}

void setPixel(int x, int y, int z, int value) {
  //make the center the origin
  z = z * -1;
  y += 1;
  x += 1;
  z += 1;

  int column = x + (z * 3);
  int row = y;

  int index = column + (row * 9);

  pixels[index] = value;
}

void lightLED(int x, int y, int z, int delayMS) {
  //make the center the origin
  z = z * -1;
  y += 1;
  x += 1;
  z += 1;
  
  int count = 2500;
  int column = x + (z * 3);
  int row = y;
  
  digitalWrite(columns[column], HIGH);
  digitalWrite(rows[y], HIGH);
  delay(delayMS);
  digitalWrite(columns[column], LOW);
  digitalWrite(rows[y], LOW);
}

void allOff() {
  for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
    digitalWrite(columns[columnIndex], LOW);
  }
  
  for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
    digitalWrite(rows[rowIndex], LOW);
  }
}

void lightColumnByColumn() {
  int count = 2500;
  for (int i = 0; i < count; i++) {
    lightColumn1();
  }
  for (int i = 0; i < count; i++) {
    lightColumn2();
  }
  for (int i = 0; i < count; i++) {
    lightColumn3();
  }
  for (int i = 0; i < count; i++) {
    lightColumn2();
  }
  for (int i = 0; i < count; i++) {
    lightColumn1();
  }

}

void lightColumn1() {
  for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
    digitalWrite(rows[rowIndex], HIGH);
    for (int columnIndex = 0; columnIndex < 3; columnIndex++) {
      digitalWrite(columns[columnIndex], HIGH);
    }
    digitalWrite(rows[rowIndex], LOW);
  }

  for (int columnIndex = 0; columnIndex < 3; columnIndex++) {
    digitalWrite(columns[columnIndex], LOW);
  }
}

void lightColumn2() {
  for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
    digitalWrite(rows[rowIndex], HIGH);
    for (int columnIndex = 3; columnIndex < 6; columnIndex++) {
      digitalWrite(columns[columnIndex], HIGH);
    }
    digitalWrite(rows[rowIndex], LOW);
  }

  for (int columnIndex = 3; columnIndex < 6; columnIndex++) {
    digitalWrite(columns[columnIndex], LOW);
  }
}

void lightColumn3() {
  for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
    digitalWrite(rows[rowIndex], HIGH);
    for (int columnIndex = 6; columnIndex < 9; columnIndex++) {
      digitalWrite(columns[columnIndex], HIGH);
    }
    digitalWrite(rows[rowIndex], LOW);
  }

  for (int columnIndex = 6; columnIndex < 9; columnIndex++) {
    digitalWrite(columns[columnIndex], LOW);
  }
}

void oneByOne(int msDelay) {
  for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
    digitalWrite(rows[rowIndex], HIGH);
    for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
      digitalWrite(columns[columnIndex], HIGH);
      delay(msDelay);
      digitalWrite(columns[columnIndex], LOW);
    }
    digitalWrite(rows[rowIndex], LOW);
  }
}

void lightAllOneAtATime() {
  for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
    digitalWrite(rows[rowIndex], HIGH);
    for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
      digitalWrite(columns[columnIndex], HIGH);
      //delay(1);
      //delayMicroseconds(800);
      digitalWrite(columns[columnIndex], LOW);
    }
    digitalWrite(rows[rowIndex], LOW);
  }
}

void lightAllOneLayerAtATime() {
  for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
      digitalWrite(columns[columnIndex], HIGH);
    }
  for (int i = 0; i < 200; i++) {
    for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
      digitalWrite(rows[rowIndex], HIGH);
      delay(5);
      digitalWrite(rows[rowIndex], LOW);
    }
  }

  for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
    digitalWrite(columns[columnIndex], LOW);
  }
}

void lightLayerByLayer() {
  for (int rowIndex = 0; rowIndex < 3; rowIndex++) {
    digitalWrite(rows[rowIndex], HIGH);
    for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
      digitalWrite(columns[columnIndex], HIGH);
    }
    delay(250);
    for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
      digitalWrite(columns[columnIndex], LOW);
    }
    digitalWrite(rows[rowIndex], LOW);
  }

  for (int rowIndex = 1; rowIndex >= 0; rowIndex--) {
    digitalWrite(rows[rowIndex], HIGH);
    for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
      digitalWrite(columns[columnIndex], HIGH);
    }
    delay(250);
    for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
      digitalWrite(columns[columnIndex], LOW);
    }
    digitalWrite(rows[rowIndex], LOW);
  }
}

